# Using Keys with Ordered Data

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/using-keys-with-ordered-data`.*

---

When and how to add DeltaXML "keys" to your input data in order to have more control over the matching process

This document describes how to run the sample. For concept details see: [Using Keys with Ordered Data](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/using-keys-with-ordered-data)

## Running the Sample
The sample can be run via a *run.bat* batch file, so long as this is issued from the sample directory. Alternatively, the commands can be executed directly:

	..\..\bin\deltaxml.exe compare ordered documentA.xml documentB.xml unkeyed-result.xml
	..\..\bin\deltaxml.exe compare ordered documentA-keyed.xml documentB-keyed.xml keyed-result.xml
	
# **Note - .NET support has been deprecated as of version 10.0.0 **